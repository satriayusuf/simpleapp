import { createRouter, createWebHistory } from "vue-router";

import CurrentPage from "@/views/CurrentPage.vue";
import LoginPage from "@/views/LoginPage.vue";
import UserDashboardPage from "@/views/user/UserDashboardPage.vue";
import insertDataPage from "@/views/user/InsertDataPage.vue";
import InsertItemPage from "@/views/user/InsertItemPage.vue";

const routes = [
  {
    path: "/",
    component: CurrentPage,
  },
  {
    path: "/login",
    component: LoginPage,
  },
  {
    path: "/dashboard",
    component: UserDashboardPage,
  },
  {
    path: "/tambah/data",
    component: insertDataPage,
  },
  {
    path: "/tambah/item/:id",
    component: InsertItemPage,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
