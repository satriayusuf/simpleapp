import { createApp } from "vue";
import App from "./App.vue";
import routes from "@/routes/index.js";
import axios from "axios";
import VueAxios from "vue-axios";
import interceptor from "@/service/interceptor";

createApp(App).use(routes, VueAxios, axios, interceptor).mount("#app");
